@extends('layouts.main')

@section('content')
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
@endsection

@push('scripts')
<script type="text/javascript">
var test = 1;
</script>

@endpush

@section('title', 'Ini judul')
